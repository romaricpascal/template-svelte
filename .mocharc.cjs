module.exports = {
  'watch-files': ['src/**/*.js'],
  spec: 'src/**/*.test.js',
  require: '.mocha_setup.mjs',
};
