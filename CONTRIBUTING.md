# Contributing

## Installation

Before anything, you'll need the right version of Node on your machine. Easiest way is to use [NVM](https://github.com/nvm-sh/nvm), which will pick up the version in [the `.nvmrc` file](.nvmrc).

After cloning the project, install [its dependencies](package.json) with NPM:

```sh
npm install
```

Once installed, make sure [tests](#testing) run OK on your machine by running

```sh
npm run test
```

At that point, you're good to go for adding new features, fixing bugs...

## Project structure

Source files are within the `/src` folder, containing two main entry points:

* `index.js`: The root ES Module exposing the library's features as named exports
* `browser.js`: Entry point for UMD exports that can be easily consumed by older browsers. Default is just a packaging of all `index.js` exports into an object, but it can be used to attach these exports to the library's main function or do some more restructuring.

## Build

Build is handled by [Rollup](https://www.rollupjs.org/guide/en/). It'll bundle the library in 4 versions:

* an ES Module bundle, unminified
* an ES Module bundle, minified
* an UMD bundle, unminified (for older browsers)
* an UMD bundle, minified.

File and export name are base on the package's metadata from `package.json`, providing a unique place to edit them.

## Testing

Javascript code can be tested two ways:

* [Cypress](https://www.cypress.io/) is configured for running tests in an actual browser, either for end to end testing or because feature require specific browser features like `MutationObserver`, `InteractionObserver`
  Cypress tests are suffixed by `.spec.js` and can be put anywhere in the `src` folder (or in the `cypress/integration` folder).
* [Mocha](https://mochajs.org/) is setup to run faster tests on features that make use of browser features available in JSDOM or no use of browser features at all. [`window`](https://github.com/lukechilds/window#universal-testing-pattern) can help getting a JSDOM environment if necessary. Those tests are suffixed by `.test.js` and can be put anywhere in the `src` folder.

For better portability, tests are set as close as the code they test:

```txt
- src
|-- module.js <-- File with the feature under test
|-- __tests__
|---- module.spec.js <-- Cypress tests that'll run in an actual browser
|---- module.test.js <-- Mocha tests that'll run without a browser
```

If test list grows and multiple files become necessary for clarity, a BEM like naming scheme can help keep things tidy: `module__test_group_1.test.js`, `module__test_group_2.test.js`,...

## Linting

Code formatting is enforced with [Prettier](https://prettier.io). It offers [integrations with the most popular editors](https://prettier.io/docs/en/editors.html) if you'd like formatting in your editor. The code will be formatted through [Git Hooks](#git-hooks) before each commit though.

Further linting is provided by:

* [ESLint](https://eslint.org) for the JavaScript code. Configuration is in [`.eslintrc.cjs`](.eslintrc.cjs) and ignored files in [`.eslintignore`](.eslintignore)

### Punctually disabling linting

Linters should be followed most of the time as they ensure a coherent codebase. There'll be time where you'll have Very Good Reasons™ to deviate from their recommendations (not "I'm in a hurry", nor "Who cares?", though, sorry). When that happen both provide ways to **temporarilly** disable them:
[ESLint's inline comments](https://eslint.org/docs/user-guide/command-line-interface#-no-inline-config).

When disabling the linters:

* be as specific as possible: only disable specific rules for specific lines or blocks of code (making sure to re-enable them afterwards if necessary)
* document the reason for disabling the linters

### Running the linters manually

Running the linters will happen [automatically before commit](#git-hooks). If you'd like to run them manually you can run:

```sh
npm run lint
```

Some linting errors can be fixed automatically by the linter. When running it manually, use the `--fix` option let the linter take care of those. This'll leave you only issue requiring human intervention.

```sh
npm run lint -- --fix
```

## Git hooks

To ensure a consistent codebase and enforce some best practices, code will be linted and formated before commit, thanks to [husky](https://typicode.github.io/husky) and [lint-staged](https://github.com/okonet/lint-staged#readme).

If you need to bypass the Git hooks you can use the `--no-verify` option when commiting. But please use it sparingly and for Very Good Reasons™.
