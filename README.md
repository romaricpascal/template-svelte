# Project template - Svelte app

Template project to use with `degit` for quickly scaffolding new projects.

## Table of contents

* [Usage](#usage)
* [Contributing](#contributing)

## Usage

```sh
npx tiged gitlab:romaricpascal/template-svelte
```

Then:

* \[ ] Update `template-svelte` inside `package.json` to actual library name

  ```sh
  (            
    PROJECT_NAME=NEW_PROJECT_NAME;
    sed -i'.bak' "s/template-svelte/$PROJECT_NAME/" package.json;
    rm package.json.bak
  )
  ```

* \[ ] Update `README.md` with relevant info

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)
