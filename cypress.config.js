import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    setupNodeEvents() {
      // implement node event listeners here
    },
    specPattern: '**/*.cy.{js,cjs,mjs}',
    supportFile: false,
  },
});
