module.exports = {
  '*.{js,cjs,mjs,svelte}': ['eslint --fix', 'prettier'],
  '*.md'(filenames) {
    return filenames.map((filename) => `remark ${filename} --output`);
  },
};
